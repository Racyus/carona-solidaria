package com.example.caronasolidaria.fragments;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;

import com.example.caronasolidaria.R;
import com.example.caronasolidaria.activities.AddressActivity;
import com.example.caronasolidaria.models.Latlog;
import com.example.caronasolidaria.models.MyUserAddress;
import com.google.firebase.firestore.GeoPoint;
import com.here.android.mpa.common.GeoCoordinate;
import com.here.android.mpa.search.Address;

public class MapMenuFragment extends DialogFragment {

    public static final String MAP_MENU_TAG = "MAP_MENU";

    public static final String EXTRA_HOME = "SERIALIZABLE_HOME";
    public static final String EXTRA_INSTITUTION = "SERIALIZABLE_INSTITUTION";
    private static final String EXTRA_ITEM = "SERIALIZABLE_ITEM";
    private static final String EXTRA_POSITION = "ITEM_POSITION";

    private TextView address = null;
    private TextView houseNumber = null;
    private TextView city = null;
    private TextView state = null;
    private TextView country = null;

    private void bridge(View view) {
        address = view.findViewById(R.id.mm_address);
        houseNumber = view.findViewById(R.id.mm_house_number);
        city = view.findViewById(R.id.mm_city);
        state = view.findViewById(R.id.mm_state);
        country = view.findViewById(R.id.mm_country);
    }

    private void fillCard(MyUserAddress myUserAddress) {
        address.setText(myUserAddress.getStreet());
        houseNumber.setText(myUserAddress.getHouseNumber());
        city.setText(myUserAddress.getCity());
        state.setText(myUserAddress.getState());
        country.setText(myUserAddress.getCountryName());
    }

    public MapMenuFragment() {
    }

    private static MyUserAddress addressToUserAddress(Address adObj, GeoCoordinate geoCoordinate) {
        MyUserAddress myUserAddress = new MyUserAddress();
        myUserAddress.setStreet(adObj.getStreet());
        myUserAddress.setState(adObj.getState());
        myUserAddress.setHouseNumber(adObj.getHouseNumber());
        myUserAddress.setPostalCode(adObj.getPostalCode());
        myUserAddress.setCity(adObj.getCity());
        myUserAddress.setCountryName(adObj.getCountryName());
        myUserAddress.setLatlog(new Latlog(geoCoordinate.getLatitude(),geoCoordinate.getLongitude()));
        return myUserAddress;
    }

    public static MapMenuFragment newInstance(Address adObj, GeoCoordinate geoCoordinate, int position) {
        MyUserAddress myUserAddress = addressToUserAddress(adObj, geoCoordinate);
        MapMenuFragment dialog = new MapMenuFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(EXTRA_ITEM, myUserAddress);
        bundle.putInt(EXTRA_POSITION, position);
        dialog.setArguments(bundle);
        return dialog;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(final Bundle savedInstanceState) {
        final Context context = getActivity();
        final Bundle bundle = getArguments();

        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        LayoutInflater inflater = LayoutInflater.from(context);
        @SuppressLint("InflateParams") View view = inflater.inflate(R.layout.fragment_map_menu, null, true);
        builder.setView(view);

        assert bundle != null;
        setView(view, bundle);

        return builder.create();
    }

    private void setView(final View view, final Bundle bundle) {
        final MyUserAddress myUserAddress = (MyUserAddress) bundle.get(EXTRA_ITEM);
        bridge(view);

        assert myUserAddress != null;
        fillCard(myUserAddress);

        Button cancel = view.findViewById(R.id.mm_cancel);
        cancel.setOnClickListener(v -> dismiss());

        Button house = view.findViewById(R.id.mm_house);
        house.setOnClickListener(v -> goToAddressAcitivity(v, myUserAddress, EXTRA_HOME));

        Button institution = view.findViewById(R.id.mm_institution);
        institution.setOnClickListener(v ->  goToAddressAcitivity(v, myUserAddress, EXTRA_INSTITUTION));
    }

    private void goToAddressAcitivity(final View view,MyUserAddress myUserAddress, final String TAG) {
        Intent intent = new Intent(view.getContext(), AddressActivity.class);
        intent.putExtra(TAG, myUserAddress);
        dismiss();
        startActivity(intent);
    }

}
