package com.example.caronasolidaria.holders;

import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.example.caronasolidaria.R;

public class CsRequestCard extends CsBaseInfoCard {

    private Button accept = null;
    private Button refuse = null;

    public CsRequestCard(@NonNull View itemView) {
        super(itemView);
        accept = itemView.findViewById(R.id.card_csreq_accept);
        refuse = itemView.findViewById(R.id.card_csreq_refuse);
    }

    public Button getAccept() {
        return accept;
    }

    public void setAccept(Button accept) {
        this.accept = accept;
    }

    public Button getRefuse() {
        return refuse;
    }

    public void setRefuse(Button refuse) {
        this.refuse = refuse;
    }
}
