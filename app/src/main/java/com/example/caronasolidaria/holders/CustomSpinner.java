package com.example.caronasolidaria.holders;

import android.widget.ImageView;
import android.widget.TextView;

import com.example.caronasolidaria.R;

public class CustomSpinner {

    private String spinnerTextView = null;
    private Integer spinnerImageView = null;

    public CustomSpinner(Integer spinnerImageView, String spinnerTextView) {
        this.spinnerImageView = spinnerImageView;
        this.spinnerTextView = spinnerTextView;
    }

    public CustomSpinner(String spinnerTextView) {
        this.spinnerImageView = R.drawable.ball_icon;
        this.spinnerTextView = spinnerTextView;
    }

    public Integer getSpinnerImageView() {
        return spinnerImageView;
    }

    public void setSpinnerImageView(Integer spinnerImageView) {
        this.spinnerImageView = spinnerImageView;
    }

    public String getSpinnerTextView() {
        return spinnerTextView;
    }

    public void setSpinnerTextView(String spinnerTextView) {
        this.spinnerTextView = spinnerTextView;
    }
}
