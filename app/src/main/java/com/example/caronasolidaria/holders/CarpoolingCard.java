package com.example.caronasolidaria.holders;


import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.caronasolidaria.R;

public class CarpoolingCard extends RecyclerView.ViewHolder {

    private Button request = null;
    private Button delete = null;

    private TextView userCity;
    private TextView comesAndGoes;
    private TextView Institution;
    private TextView InstitutionCity;
    private TextView name;
    private TextView type;
    private TextView period;

    public CarpoolingCard(@NonNull View itemView) {
        super(itemView);
        userCity = itemView.findViewById(R.id.card_carpooling_user_city);
        comesAndGoes = itemView.findViewById(R.id.card_carpooling_comes_and_goes);
        Institution = itemView.findViewById(R.id.card_carpooling_institution);
        InstitutionCity = itemView.findViewById(R.id.card_carpooling_institution_city);
        name = itemView.findViewById(R.id.card_carpooling_name);
        type = itemView.findViewById(R.id.card_carpooling_type);
        period = itemView.findViewById(R.id.card_carpooling_period);

        request = itemView.findViewById(R.id.card_carpooling_request_contact);
        delete = itemView.findViewById(R.id.card_carpooling_delete);
    }

    public int getAdapterLocal() {
        return getAdapterPosition();
    }

    public Button getRequest() {
        return request;
    }

    public Button getDelete() {
        return delete;
    }

    public TextView getUserCity() {
        return userCity;
    }

    public TextView getComesAndGoes() {
        return comesAndGoes;
    }

    public TextView getInstitution() {
        return Institution;
    }

    public TextView getInstitutionCity() {
        return InstitutionCity;
    }

    public TextView getName() {
        return name;
    }

    public TextView getType() {
        return type;
    }

    public TextView getPeriod() {
        return period;
    }
}
