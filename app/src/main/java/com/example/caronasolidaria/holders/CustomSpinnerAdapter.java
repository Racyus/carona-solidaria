package com.example.caronasolidaria.holders;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.caronasolidaria.R;

import java.util.ArrayList;

public class CustomSpinnerAdapter extends ArrayAdapter<CustomSpinner> {

    public CustomSpinnerAdapter(@NonNull Context context, ArrayList<CustomSpinner> list) {
        super(context, 0, list);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return customView(position, convertView, parent);
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return customView(position, convertView, parent);
    }

    public View customView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.cs_spinner, parent, false);
        }
        CustomSpinner items = getItem(position);
        ImageView imageView = convertView.findViewById(R.id.iv_spinner);
        TextView textView = convertView.findViewById(R.id.tv_spinner);
        if(items!=null){
            imageView.setImageResource(items.getSpinnerImageView());
            textView.setText(items.getSpinnerTextView());
        }
        return convertView;
    }
}
