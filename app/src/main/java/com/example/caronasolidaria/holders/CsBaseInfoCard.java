package com.example.caronasolidaria.holders;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.caronasolidaria.R;


public class CsBaseInfoCard extends RecyclerView.ViewHolder {

    private TextView userCity;
    private TextView course;
    private TextView institution;
    private TextView institutionCity;
    private TextView name;
    private TextView phone;
    private TextView ra;

    public CsBaseInfoCard(@NonNull View itemView) {
        super(itemView);
        userCity = itemView.findViewById(R.id.card_csreq_city);
        course = itemView.findViewById(R.id.card_csreq_course);
        institution = itemView.findViewById(R.id.card_csreq_institution);
        institutionCity = itemView.findViewById(R.id.card_csreq_institution_city);
        name = itemView.findViewById(R.id.card_csreq_name);
        phone = itemView.findViewById(R.id.card_csreq_phone_number);
        ra = itemView.findViewById(R.id.card_csreq_ra);
    }

    public TextView getUserCity() {
        return userCity;
    }

    public void setUserCity(TextView userCity) {
        this.userCity = userCity;
    }

    public TextView getCourse() {
        return course;
    }

    public void setCourse(TextView course) {
        this.course = course;
    }

    public TextView getInstitution() {
        return institution;
    }

    public void setInstitution(TextView institution) {
        this.institution = institution;
    }

    public TextView getInstitutionCity() {
        return institutionCity;
    }

    public void setInstitutionCity(TextView institutionCity) {
        this.institutionCity = institutionCity;
    }

    public TextView getName() {
        return name;
    }

    public void setName(TextView name) {
        this.name = name;
    }

    public TextView getPhone() {
        return phone;
    }

    public void setPhone(TextView phone) {
        this.phone = phone;
    }

    public TextView getRa() {
        return ra;
    }

    public void setRa(TextView ra) {
        this.ra = ra;
    }
}
