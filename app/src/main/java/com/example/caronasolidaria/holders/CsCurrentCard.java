package com.example.caronasolidaria.holders;

import android.view.View;
import android.widget.Button;

import androidx.annotation.NonNull;

import com.example.caronasolidaria.R;

public class CsCurrentCard extends CsBaseInfoCard {

    private Button cancel;

    public CsCurrentCard(@NonNull View itemView) {
        super(itemView);
        cancel = itemView.findViewById(R.id.card_csreq_cancel);
        cancel.setVisibility(View.VISIBLE);

        itemView.findViewById(R.id.card_csreq_accept).setVisibility(View.GONE);
        itemView.findViewById(R.id.card_csreq_refuse).setVisibility(View.GONE);
    }

    public Button getCancel() {
        return cancel;
    }

    public void setCancel(Button cancel) {
        this.cancel = cancel;
    }
}
