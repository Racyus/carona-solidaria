package com.example.caronasolidaria.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.caronasolidaria.R;
import com.example.caronasolidaria.adapters.CarpoolingAdapter;
import com.example.caronasolidaria.models.Aluno;
import com.example.caronasolidaria.models.Carpooling;
import com.example.caronasolidaria.models.MyUserAddress;
import com.example.caronasolidaria.models.UserClient;
import com.example.caronasolidaria.models.Vehicle;
import com.example.caronasolidaria.utils.CsCallback;
import com.getbase.floatingactionbutton.FloatingActionButton;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class SearchActivity extends BaseActivity implements CsCallback {

    private static final String TAG = "SearchActivity";

    public static final String CARPOOL = "carp";
    public static final String UID = "uid";
    public static final String VEHICLE1 = "veh1";
    public static final String VEHICLE2 = "veh2";

    private ImageButton back = null;
    private FloatingActionButton offer = null;

    private Carpooling myCarpooling = null;

    private static Context context = null;

    private DocumentReference alunoReference = null;
    private DocumentReference userHouseReference = null;
    private DocumentReference userInstitutionReference = null;
    private DocumentReference vehicle1Reference = null;
    private DocumentReference vehicle2Reference = null;

    private volatile Aluno aluno = null;
    private volatile MyUserAddress userHouse = null;
    private volatile MyUserAddress userInstitution = null;
    private volatile Vehicle vehicle1 = null;
    private volatile Vehicle vehicle2 = null;

    private RecyclerView cardRecyclerView = null;
    private RecyclerView.Adapter mReAdapter = null;

    private RecyclerView myCardRecyclerView = null;
//    private RecyclerView.LayoutManager mReManager= null;


    private EditText filterLayout = null;

    private void bridge() {
        back = findViewById(R.id.sa_back);
        offer = findViewById(R.id.sa_offer);

        alunoReference = FirebaseFirestore.getInstance().collection(getString(R.string.collection_users))
                .document(getUid())
                .collection(getString(R.string.collection_user_info))
                .document(getString(R.string.user_info));
        userHouseReference = FirebaseFirestore.getInstance()
                .collection(getString(R.string.collection_users))
                .document(getUid())
                .collection(getString(R.string.collection_address))
                .document("HOME");
        userInstitutionReference = FirebaseFirestore.getInstance()
                .collection(getString(R.string.collection_users))
                .document(getUid())
                .collection(getString(R.string.collection_address))
                .document("INSTITUTION");
        vehicle1Reference = FirebaseFirestore.getInstance()
                .collection(getString(R.string.collection_users))
                .document(getUid())
                .collection(getString(R.string.collection_vehicles))
                .document("Vehicle1");
        vehicle2Reference = FirebaseFirestore.getInstance()
                .collection(getString(R.string.collection_users))
                .document(getUid())
                .collection(getString(R.string.collection_vehicles))
                .document("Vehicle2");

        filterLayout = findViewById(R.id.sa_filter_layout);
    }

    private void getAllObjects() {
        final SearchActivity context = this;
        showProgressDialog();
        alunoReference.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    context.setAluno(Objects.requireNonNull(task.getResult()).toObject(Aluno.class));
                }
            }
        });

        userHouseReference.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    context.setUserHouse(Objects.requireNonNull(task.getResult()).toObject(MyUserAddress.class));
                }
            }
        });

        userInstitutionReference.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    context.setUserInstitution(Objects.requireNonNull(task.getResult()).toObject(MyUserAddress.class));
                }
            }
        });

        vehicle1Reference.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    context.setVehicle1(Objects.requireNonNull(task.getResult()).toObject(Vehicle.class));
                }
            }
        });

        vehicle2Reference.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    context.setVehicle2(Objects.requireNonNull(task.getResult()).toObject(Vehicle.class));
                }
            }
        });

    }

    private Carpooling getCarpooling() {
        return new Carpooling(FirebaseAuth.getInstance().getCurrentUser().getDisplayName()
                , aluno, userHouse, userInstitution);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        customStatusBar();

        if (context == null) {
            context = this;
        }

        bridge();
        getAllObjects();

        hideSoftKeyboard();

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        offer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                showCarpoolingFragment(CarpoolingDialogFragment.CREATE_TAG, -1);
                startActivityResult();
            }
        });

    }

    public static void updateScreen() {
        SearchActivity sa = (SearchActivity) context;
        sa.getTravels();
    }

    @Override
    protected void onResume() {
        super.onResume();
        getTravels();
    }


    private void getTravels() {
        Log.d(TAG, "getTravels: chamou");
        CollectionReference carpoolsRef = FirebaseFirestore.getInstance().collection(getString(R.string.collection_carpoolings));

        cardRecyclerView = findViewById(R.id.sa_carpooling_cards);
        cardRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        myCardRecyclerView = findViewById(R.id.sa_your_carpool);
        myCardRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        carpoolsRef.get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
            @Override
            public void onSuccess(final QuerySnapshot documents) {
                if (documents.isEmpty()) {
                    showToast("Não há viagens disponíveis");
                } else {
                    final List<Carpooling> list = new ArrayList<>();
                    final List<Carpooling> myList = new ArrayList<>();

                    for(Carpooling c :documents.toObjects(Carpooling.class)){
                        if (c.getName().equals(FirebaseAuth.getInstance().getCurrentUser().getDisplayName())) {
                            myList.add(c);
                        } else {
                            list.add(c);
                        }
                    }

                    mReAdapter = new CarpoolingAdapter(list, new CsCallback() {
                        @Override
                        public void callbackReturn() {
                            getTravels();
                            Log.i(TAG, "callbackReturn: Chamou interno");
                        }
                    }, getCarpooling());
                    cardRecyclerView.setAdapter(mReAdapter);
                    LinearLayout layout = findViewById(R.id.sa_myCarpool);
                    if (!myList.isEmpty()) {
                        mReAdapter = new CarpoolingAdapter(myList, new CsCallback() {
                            @Override
                            public void callbackReturn() {
                                getTravels();
                                Log.i(TAG, "callbackReturn: Chamou interno doiis");
                            }
                        }, getCarpooling());
                        myCardRecyclerView.setAdapter(mReAdapter);
                        layout.setVisibility(View.VISIBLE);
                    } else {
                        layout.setVisibility(View.GONE);
                    }
                }
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.e(TAG, "onFailure: " + e.getMessage(), e);
                showToast("Erro ao procurar viagens");
            }
        });
    }

    private void startActivityResult() {
        Intent intent = new Intent(this, CreateCarpooling.class);
        Log.d(TAG, "startActivityResult: carpool" + getCarpooling());
        intent.putExtra(CARPOOL, getCarpooling());
        intent.putExtra(UID, getUid());
        intent.putExtra(VEHICLE1, vehicle1);
        intent.putExtra(VEHICLE2, vehicle2);
        startActivityForResult(intent, 1);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if (resultCode == RESULT_CANCELED) {
                Log.d(TAG, "onActivityResult: creating carpooling canceled.");
            }
            if (resultCode == RESULT_OK) {
                getTravels();
            }
        }
    }

    public void setAluno(Aluno aluno) {
        hideProgressDialog();
        this.aluno = aluno;
    }

    public void setUserHouse(MyUserAddress userHouse) {
        hideProgressDialog();
        this.userHouse = userHouse;
    }

    public void setUserInstitution(MyUserAddress userInstitution) {
        hideProgressDialog();
        this.userInstitution = userInstitution;
    }

    public void setVehicle1(Vehicle vehicle1) {
        hideProgressDialog();
        this.vehicle1 = vehicle1;
    }

    public void setVehicle2(Vehicle vehicle2) {
        hideProgressDialog();
        this.vehicle2 = vehicle2;
    }

    public Aluno getAluno() {
        return aluno;
    }

    public MyUserAddress getUserHouse() {
        return userHouse;
    }

    public MyUserAddress getUserInstitution() {
        return userInstitution;
    }

    public Vehicle getVehicle1() {
        return vehicle1;
    }

    public Vehicle getVehicle2() {
        return vehicle2;
    }

    @Override
    public void callbackReturn() {
        getTravels();
        Log.i(TAG, "callbackReturn: Chamou");
    }
}

