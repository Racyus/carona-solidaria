package com.example.caronasolidaria.activities;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.caronasolidaria.R;
import com.example.caronasolidaria.adapters.CsCurrentAdapter;
import com.example.caronasolidaria.adapters.CsRequestAdapter;
import com.example.caronasolidaria.models.CsRequest;
import com.example.caronasolidaria.utils.CsCallback;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

public class InfoActivity extends BaseActivity {

    private static final String TAG = "InfoActivity";

    private ScrollView scrollOne = null;
    private ScrollView scrollTwo = null;
    private ScrollView scrollThree = null;

    private TextView textViewTitle = null;
    private ImageView imageOne = null;
    private ImageView imageTwo = null;
    private ImageView imageThree = null;

    private RecyclerView requestRecycler = null;
    private RecyclerView currentRecycler = null;


    private ImageButton back = null;

    public void buttonOne() {
        textViewTitle.setText(getString(R.string.travel_in_progress));
        imageOne.setBackground(getDrawable(R.drawable.corner_white_stroke));
        imageTwo.setBackground(getDrawable(R.color.fui_transparent));
        imageThree.setBackground(getDrawable(R.color.fui_transparent));
        scrollOne.setVisibility(View.VISIBLE);
        scrollTwo.setVisibility(View.GONE);
        scrollThree.setVisibility(View.GONE);
        getRequests();
    }

    public void buttonTwo() {
        textViewTitle.setText(getString(R.string.pending_requests));
        imageOne.setBackground(getDrawable(R.color.fui_transparent));
        imageTwo.setBackground(getDrawable(R.drawable.corner_white_stroke));
        imageThree.setBackground(getDrawable(R.color.fui_transparent));
        scrollOne.setVisibility(View.GONE);
        scrollTwo.setVisibility(View.VISIBLE);
        scrollThree.setVisibility(View.GONE);
        getRequests();
    }

    private void brigde() {
        back = findViewById(R.id.ia_back);

        scrollOne = findViewById(R.id.ai_scroll_one);
        scrollTwo = findViewById(R.id.ai_scroll_two);
        scrollThree = findViewById(R.id.ai_scroll_three);

        textViewTitle = findViewById(R.id.ai_text_view_title);
        imageOne = findViewById(R.id.ai_image_view_one);
        imageTwo = findViewById(R.id.ai_image_view_two);
        imageThree = findViewById(R.id.ai_image_view_three);

        requestRecycler = findViewById(R.id.ai_recycler_two);
        requestRecycler.setLayoutManager(new LinearLayoutManager(this));

        currentRecycler = findViewById(R.id.ai_recycler_one);
        currentRecycler.setLayoutManager(new LinearLayoutManager(this));

        imageOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonOne();
            }
        });

        imageTwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonTwo();
            }
        });

        imageThree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                textViewTitle.setText(getString(R.string.notifications));
                imageOne.setBackground(getDrawable(R.color.fui_transparent));
                imageTwo.setBackground(getDrawable(R.color.fui_transparent));
                imageThree.setBackground(getDrawable(R.drawable.corner_white_stroke));
                scrollOne.setVisibility(View.GONE);
                scrollTwo.setVisibility(View.GONE);
                scrollThree.setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);
        customStatusBar();

        brigde();

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    protected void onResume() {
        Log.d(TAG, "onResume: resumou");
        super.onResume();
        getRequests();
        buttonTwo();
    }

    public void getRequests() {
        CollectionReference requestRf = FirebaseFirestore.getInstance()
                .collection(getString(R.string.collection_carpoolings))
                .document(getUid())
                .collection(getString(R.string.collection_requests));

        requestRf.get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
            @Override
            public void onSuccess(QuerySnapshot documents) {
                final List<CsRequest> list = new ArrayList<>();
                if (!documents.isEmpty()) {
                    documents.toObjects(CsRequest.class).forEach(new Consumer<CsRequest>() {
                        @Override
                        public void accept(CsRequest csRequest) {
                            list.add(csRequest);
                            Log.d(TAG, "accept: " + csRequest);
                        }
                    });
                }
                RecyclerView.Adapter adapter = new CsRequestAdapter(list, new CsCallback() {
                    @Override
                    public void callbackReturn() {
                        getRequests();
                    }
                });
                Log.d(TAG, "onSuccess: " + adapter.getItemId(0));
                requestRecycler.setAdapter(adapter);
            }
        });

        CollectionReference acceptRf = FirebaseFirestore.getInstance()
                .collection(getString(R.string.collection_carpoolings))
                .document(getUid())
                .collection(getString(R.string.collection_current));
        acceptRf.get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
            @Override
            public void onSuccess(QuerySnapshot documents) {
                final List<CsRequest> list = new ArrayList<>();
                if (!documents.isEmpty()) {
                    documents.toObjects(CsRequest.class).forEach(new Consumer<CsRequest>() {
                        @Override
                        public void accept(CsRequest csRequest) {
                            list.add(csRequest);
                            Log.d(TAG, "accept: " + csRequest);
                        }
                    });
                }
                RecyclerView.Adapter adapter = new CsCurrentAdapter(list, new CsCallback() {
                    @Override
                    public void callbackReturn() {
                        getRequests();
                    }
                });
                Log.d(TAG, "onSuccess: " + adapter.getItemId(0));
                currentRecycler.setAdapter(adapter);

            }
        });
    }

}
