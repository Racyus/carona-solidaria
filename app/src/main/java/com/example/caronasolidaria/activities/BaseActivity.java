package com.example.caronasolidaria.activities;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.caronasolidaria.R;
import com.example.caronasolidaria.models.Carpooling;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;

import java.util.Objects;

public class BaseActivity extends AppCompatActivity {

    private static final String TAG = "BaseActivity";

    @SuppressWarnings("deprecation")
    private ProgressDialog mProgressDialog;

    public void customStatusBar() {
        getWindow().setStatusBarColor(getColor(R.color.cs_blue));
        View decor = getWindow().getDecorView();
        decor.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
    }

    public void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setMessage(getString(R.string.loading));
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setCancelable(false);
        }

        mProgressDialog.show();
    }

    public void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    public String getUid() {
        return Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getUid();
    }

    public void hideSoftKeyboard() {
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }


    public void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    public void listenDbTopic() {
        CollectionReference topic = FirebaseFirestore.getInstance()
                .collection(getString(R.string.collection_carpoolings))
                .document(getUid())
                .collection(getString(R.string.collection_request_notification));

        final AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle("Viagem encontrada");
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Refuse", (dialog, which) -> {

        });


        topic.addSnapshotListener((value, e) -> {
            if (e != null) {
                Log.w(TAG, "Listen failed.", e);
                return;
            }

            if (value != null) {
                for (QueryDocumentSnapshot doc : value) {
                    Carpooling cp = doc.toObject(Carpooling.class);
                    if(cp.getUid().equals(getUid())){
                        alertDialog.setTitle("Aguardando sua viagem a ser aceita.");
                    }else{
                        alertDialog.setTitle("Viagem encontrada");
                        alertDialog.setMessage("Nome: " + cp.getName()+ System.lineSeparator()
                                +"Cidade"+ cp.getUserHouse().getCity());
                        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Aceitar", (dialog, which) -> {

                        });
                    }

                    alertDialog.show();
                    showToast(doc.getId());
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onStop() {
        super.onStop();
        customStatusBar();
        hideProgressDialog();
    }
}