package com.example.caronasolidaria.activities;

import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.caronasolidaria.R;
import com.example.caronasolidaria.fragments.MapMenuFragment;
import com.example.caronasolidaria.models.MyUserAddress;
import com.example.caronasolidaria.models.UserClient;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.Objects;


public class AddressActivity extends BaseActivity {

    private static final String TAG = "AddressActivity";

    private static final String HOME = "HOME";
    private static final String INSTITUTION = "INSTITUTION";
    private String selected = null;

    private MyUserAddress myUserAddress = null;

    private CollectionReference userAddressReference = null;
    private ImageButton back = null;
    private Button save = null;
    private Button home = null;
    private Button institution = null;

    private TextView cep = null;
    private TextView addressEndereco = null;
    private TextView number = null;
    private TextView city = null;
    private TextView state = null;
    private TextView country = null;

    private void bridge() {
        userAddressReference = FirebaseFirestore.getInstance()
                .collection(getString(R.string.collection_users))
                .document(getUid())
                .collection(getString(R.string.collection_address));
        cep = findViewById(R.id.ad_cep_edit_text);
        addressEndereco = findViewById(R.id.ad_address_edit_text);
        number = findViewById(R.id.ad_number_edit_text);
        city = findViewById(R.id.ad_city_edit_text);
        state = findViewById(R.id.ad_state_edit_text);
        country = findViewById(R.id.ad_country_edit_text);

        back = findViewById(R.id.ad_back);
        save = findViewById(R.id.ad_save_button);
        home = findViewById(R.id.ad_home);
        institution = findViewById(R.id.ad_institution);
    }


    private void getExtras(){
            if (getIntent().getExtras()!=null){
                if(getIntent().getSerializableExtra(MapMenuFragment.EXTRA_HOME)!=null){
                    myUserAddress = (MyUserAddress) getIntent().getSerializableExtra(MapMenuFragment.EXTRA_HOME);
                    fillScreen(myUserAddress);
                    selected = HOME;
                    home.setBackground(getDrawable(R.drawable.corner_blue_stroke_selected));
                    institution.setBackground(getDrawable(R.drawable.corner_blue_stroke));
                }else if(getIntent().getSerializableExtra(MapMenuFragment.EXTRA_INSTITUTION)!=null){
                    myUserAddress = (MyUserAddress) getIntent().getSerializableExtra(MapMenuFragment.EXTRA_INSTITUTION);
                    fillScreen(myUserAddress);
                    selected = INSTITUTION;
                    institution.setBackground(getDrawable(R.drawable.corner_blue_stroke_selected));
                    home.setBackground(getDrawable(R.drawable.corner_blue_stroke));
                }
            }else{
                getAddressFromFirebase(HOME);
                selected = HOME;
                home.setBackground(getDrawable(R.drawable.corner_blue_stroke_selected));
                institution.setBackground(getDrawable(R.drawable.corner_blue_stroke));
            }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_address);
        showProgressDialog();

        bridge();

        back.setOnClickListener(v -> finish());

        save.setOnClickListener(v -> {
            try {
                    userAddressReference.document(selected).set(myUserAddress);
                    showToast(getString(R.string.save_success));
                    finish();
            } catch (Exception e) {
                Log.e(TAG, "onClick: " + e.getMessage(), e);
                showToast(getString(R.string.save_error));
                e.printStackTrace();
            }
        });

        home.setOnClickListener(v -> {
            showProgressDialog();
            getAddressFromFirebase(HOME);
            selected = HOME;
            home.setBackground(getDrawable(R.drawable.corner_blue_stroke_selected));
            institution.setBackground(getDrawable(R.drawable.corner_blue_stroke));
        });

        institution.setOnClickListener(v -> {
            showProgressDialog();
            getAddressFromFirebase(INSTITUTION);
            selected = INSTITUTION;
            institution.setBackground(getDrawable(R.drawable.corner_blue_stroke_selected));
            home.setBackground(getDrawable(R.drawable.corner_blue_stroke));
        });

        getExtras();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private void getAddressFromFirebase(final String selected) {
        userAddressReference.document(selected).get().addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                if (Objects.requireNonNull(task.getResult()).toObject(MyUserAddress.class) != null) {
                    myUserAddress = Objects.requireNonNull(task.getResult().toObject(MyUserAddress.class));
                    fillScreen(myUserAddress);
                    Log.d(TAG, "onComplete: pegou algo");
                } else {
                    hideProgressDialog();
                    cep.setText(getString(R.string.cep));
                    addressEndereco.setText(getString(R.string.address));
                    number.setText(getString(R.string.houseNumber));
                    city.setText(getString(R.string.city));
                    state.setText(getString(R.string.state));
                    country.setText(getString(R.string.country));
                    Log.d(TAG, "Null "+ selected+": " + ((UserClient) getApplicationContext()).getUser().getDisplayName());
                }
                Log.d(TAG, "onComplete() called with: task = [" + task.getResult().toString() + "]");
            }
        });
    }

    private void fillScreen(MyUserAddress address) {
        if (address != null) {
            cep.setText(address.getPostalCode());
            addressEndereco.setText(address.getStreet());
            number.setText(address.getHouseNumber());
            city.setText(address.getCity());
            state.setText(address.getState());
            country.setText(address.getCountryName());
        }
        hideProgressDialog();

    }

}
