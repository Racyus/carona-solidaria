package com.example.caronasolidaria.activities;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.example.caronasolidaria.R;
import com.example.caronasolidaria.utils.PermissionUtils;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;


public class SplashScreenActivity extends BaseActivity implements Runnable {

    private static final String TAG = "SplashScreenActivity";

    private static final int ERROR_DIALOG_REQUEST = 9001;

    private static final int DELAY_MILLIS = 3000;

    private ImageView logo = null;
    private ImageView boy = null;

    private void bridge(){
        logo = findViewById(R.id.sc_logo);
        boy = findViewById(R.id.sc_boy);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        bridge();
    }

    @Override
    protected void onResume() {
        super.onResume();

        customStatusBar();

        Animation fadeAnimation = AnimationUtils.loadAnimation(this, R.anim.fade_in);
        logo.startAnimation(fadeAnimation);
        boy.startAnimation(fadeAnimation);

        Handler handle = new Handler();
        handle.postDelayed(this, DELAY_MILLIS);
    }

    public boolean isServicesOK() {
        Log.d(TAG, "isServicesOK: checking Google Services.");
        int available = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(SplashScreenActivity.this);

        if (available == ConnectionResult.SUCCESS) {//available == ConnectionResult.SUCCESS
            Log.d(TAG, "isServicesOK: Google Play Services is working");
            return true;
        } else if (GoogleApiAvailability.getInstance().isUserResolvableError(available)) {
            Log.d(TAG, "isServicesOK: an error occurred but we can fix it");
            Dialog dialog = GoogleApiAvailability.getInstance().getErrorDialog(SplashScreenActivity.this, available, ERROR_DIALOG_REQUEST);
            dialog.show();
        } else {
            makeDialog("Google Services outdated");

        }
        return false;
    }

    public void makeDialog(String msg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(msg).setCancelable(false)
                .setPositiveButton("Exit", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void changeToSingInActivity() {
        Log.d(TAG, "changeToSingInActivity: changing to SingIn activity.");
        startActivity(new Intent(this, SingIn.class));
        finish();
    }

    @Override
    public void run() {
        if (isServicesOK()) {
            if (PermissionUtils.isMapsEnabled(this)) {
                changeToSingInActivity();
            }else {
                makeDialog(getApplicationContext().getResources().getText(R.string.noGpsAlertDialog).toString());
            }
        }
    }
}
