package com.example.caronasolidaria.activities;

import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.ImageButton;

import com.example.caronasolidaria.R;

public class TravelHistoricActivity extends BaseActivity {

    private ImageButton back = null;

    private void bridge(){
        back = findViewById(R.id.ha_back);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_travel_historic);

        bridge();

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_travel_historic, menu);
        return true;
    }
}
