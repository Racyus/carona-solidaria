package com.example.caronasolidaria.activities;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import com.example.caronasolidaria.R;
import com.example.caronasolidaria.models.Vehicle;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.Objects;

public class VehicleActivity extends BaseActivity {

    private static final String TAG = "VehicleActivity";

    private static final String VEHICLE1 = "Vehicle1";
    private static final String VEHICLE2 = "Vehicle2";

    private CollectionReference vehicleReference = null;
    private String selectedVehicle = null;

    private EditText model = null;
    private EditText board = null;
    private EditText color = null;
    private EditText km_expenditure = null;
    private EditText monthly_expenditure = null;

    private Button vehicle1 = null;
    private Button vehicle2 = null;
    private ImageButton backButton = null;
    private Button save = null;

    public void bridge() {
        vehicleReference = FirebaseFirestore.getInstance()
                .collection(getString(R.string.collection_users))
                .document(getUid())
                .collection(getString(R.string.collection_vehicles));

        model = findViewById(R.id.vehicle_model_editText);
        board = findViewById(R.id.vehicle_board_editText);
        color = findViewById(R.id.vehicle_color_editText);
        km_expenditure = findViewById(R.id.vehicle_km_expenditure_editText);
        monthly_expenditure = findViewById(R.id.vehicle_monthly_expenditure_editText);

        vehicle1 = findViewById(R.id.vehicle_vehicle_one);
        vehicle2 = findViewById(R.id.vehicle_vehicle_two);
        backButton = findViewById(R.id.vehicle_back_button);
        save = findViewById(R.id.vehicle_save_button);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vehicle);
        showProgressDialog();

        bridge();

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        vehicle1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showProgressDialog();
                getVehicleFromFirebase(VEHICLE1);
                vehicle1.setBackground(getDrawable(R.drawable.corner_blue_stroke_selected));
                vehicle2.setBackground(getDrawable(R.drawable.corner_blue_stroke));
            }
        });

        vehicle2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showProgressDialog();
                getVehicleFromFirebase(VEHICLE2);
                vehicle1.setBackground(getDrawable(R.drawable.corner_blue_stroke));
                vehicle2.setBackground(getDrawable(R.drawable.corner_blue_stroke_selected));
            }
        });

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    vehicleReference.document(selectedVehicle).set(getVehicle());
                    showToast(getString(R.string.save_success));
                    finish();
                } catch (Exception e) {
                    showToast(getString(R.string.save_error));
                    e.printStackTrace();
                }

            }

        });

    }

    private Vehicle getVehicle() {
        Vehicle vehicle = new Vehicle();
        vehicle.setModel(model.getText().toString());
        vehicle.setBoard(board.getText().toString());
        vehicle.setColor(color.getText().toString());
        if(km_expenditure.getText().toString().length()<3){
            vehicle.setKmExpenditure("Não inf.");
        }else vehicle.setKmExpenditure(km_expenditure.getText().toString());
        if(monthly_expenditure.getText().toString().length()<3){
            vehicle.setMonthlyExpenditure("Não inf.");
        }else vehicle.setMonthlyExpenditure(monthly_expenditure.getText().toString());
        return vehicle;
    }



    @Override
    protected void onResume() {
        super.onResume();
        getVehicleFromFirebase(VEHICLE1);
        vehicle1.setBackground(getDrawable(R.drawable.corner_blue_stroke_selected));

    }

    private void getVehicleFromFirebase(String str) {
        selectedVehicle = str;
        vehicleReference.document(str).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    if (Objects.requireNonNull(task.getResult()).toObject(Vehicle.class) != null) {
                        fillScreen(Objects.requireNonNull(task.getResult().toObject(Vehicle.class)));
                    }
                    else{
                        hideProgressDialog();
                        model.setText(null);
                        board.setText(null);
                        color.setText(null);
                        km_expenditure.setText(null);
                        monthly_expenditure.setText(null);
                        Log.d(TAG, "onComplete: null vehicle");
                    }
                }
            }
        });
    }


    private void fillScreen(Vehicle vehicle) {
        model.setText(vehicle.getModel());
        board.setText(vehicle.getBoard());
        color.setText(vehicle.getColor());
        km_expenditure.setText(vehicle.getKmExpenditure());
        monthly_expenditure.setText(vehicle.getMonthlyExpenditure());
        hideProgressDialog();
    }
}
