package com.example.caronasolidaria.activities;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.inputmethod.EditorInfo;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.FragmentManager;

import com.example.caronasolidaria.R;
import com.example.caronasolidaria.fragments.MapMenuFragment;
import com.example.caronasolidaria.models.MyUserAddress;
import com.example.caronasolidaria.services.LocationService;
import com.example.caronasolidaria.utils.PermissionUtils;
import com.firebase.ui.auth.AuthUI;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.Task;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.here.android.mpa.common.GeoBoundingBox;
import com.here.android.mpa.common.GeoCoordinate;
import com.here.android.mpa.common.MapEngine;
import com.here.android.mpa.common.OnEngineInitListener;
import com.here.android.mpa.mapping.Map;
import com.here.android.mpa.mapping.MapMarker;
import com.here.android.mpa.mapping.MapRoute;
import com.here.android.mpa.mapping.SupportMapFragment;
import com.here.android.mpa.routing.RouteManager;
import com.here.android.mpa.routing.RouteOptions;
import com.here.android.mpa.routing.RoutePlan;
import com.here.android.mpa.routing.RouteResult;
import com.here.android.mpa.search.ErrorCode;
import com.here.android.mpa.search.GeocodeRequest;
import com.here.android.mpa.search.GeocodeResult;
import com.here.android.mpa.search.Location;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;

import static com.example.caronasolidaria.utils.Constants.DEFAULT_ZOOM;
import static com.example.caronasolidaria.utils.Constants.LOCATION_PERMISSION_REQUEST_CODE;
import static com.example.caronasolidaria.utils.Constants.MAPVIEW_BUNDLE_KEY;
import static com.example.caronasolidaria.utils.Constants.PERMISSION_REQUEST_ENABLE_GPS;
import static com.example.caronasolidaria.utils.PermissionUtils.getLocationPermission;


public class MainActivityNavMap extends BaseActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    //((UserClient) getApplicationContext()).setUser(user)

    private static final String TAG = "MainActivityNavMap";
    private CollectionReference userAddressReference = null;
    private MyUserAddress localInstitution = null;


    private TextView mSearchText = null;

    private Boolean mLocationPermissionGranted = false;
    private FusedLocationProviderClient fusedLocationProviderClient = null;
    private FirebaseFirestore firebaseFirestore = null;

    private ImageButton mapMenu = null;
    private com.here.android.mpa.search.Location userGeo = null;
    private com.here.android.mpa.search.Location userSearch = null;
    private MapMarker marker = null;
    private RouteManager.Listener routeManagerListener = null;


    private static List<MapRoute> mapRoute = null;

    private SupportMapFragment mapFragment = null;
    private Map mMap = null;

    private String userName = null;
    private String email = null;

    private Integer count = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map_menu);
        Toolbar toolbar = findViewById(R.id.am_toolbar);
        setSupportActionBar(toolbar);
        bridge();
        customStatusBar();

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        hideSoftKeyboard();

        initMapEngine(savedInstanceState);

    }

    @Override
    protected void onStart() {
        super.onStart();
        mapFragment.onStart();
    }


    @Override
    protected void onResume() {
        super.onResume();
        mapFragment.onResume();
        mLocationPermissionGranted = getLocationPermission(this);
        Log.d(TAG, "onResume() called with: mapEnabled = [" + PermissionUtils.isMapsEnabled(this) + "], mLocation = [" + mLocationPermissionGranted + "]");
        if (PermissionUtils.isMapsEnabled(this) && mLocationPermissionGranted) {
            Log.d(TAG, "onResume: All permissions enabled calling method getUserDetails()");
            setupPlacesAutoComplete();

            new Timer().schedule(new TimerTask() {
                @Override
                public void run() {
                    getDeviceLocation();
                }
            }, 2000);
            getDeviceLocation();
//            startLocationService();
//            listenDbTopic();
        } else {
            Log.d(TAG, "onResume() isMapsEnabled: false");
            makeDialog(getApplicationContext().getResources().getText(R.string.noGpsAlertDialog).toString());
        }
    }

    private void bridge() {
        userAddressReference = FirebaseFirestore.getInstance()
                .collection(getString(R.string.collection_users))
                .document(getUid())
                .collection(getString(R.string.collection_address));
        Log.d(TAG, "bridge: linking resources with activity");
        if (FirebaseAuth.getInstance().getCurrentUser() != null) {
            userName = FirebaseAuth.getInstance().getCurrentUser().getDisplayName();
            email = FirebaseAuth.getInstance().getCurrentUser().getEmail();
            Log.d(TAG, "bridge: getting email of current user.");
        } else {
            Log.d(TAG, "bridge: no current user, changing to singInActivity");
            showToast("Error UserAuth");
            singOut();
        }
        firebaseFirestore = FirebaseFirestore.getInstance();
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
        mapRoute = new ArrayList<>();

        //widgets
        mSearchText = findViewById(R.id.am_places_autocomplete);
        mapMenu = findViewById(R.id.am_menu);
        mapMenu.setOnClickListener(v -> showMapMenuFragment());

        ImageButton navButton = findViewById(R.id.navigation_button);
        navButton.setOnClickListener(v -> {
            getDirections();
        });

        //Botão de pesquisa
        ImageButton searchButton = findViewById(R.id.gps_fixed);
        searchButton.setOnClickListener(v -> {
            if (userSearch != null) {
                moveCamera(userSearch, DEFAULT_ZOOM);
                mapMenu.setBackground(getDrawable(R.drawable.corner_blue));
            } else {
                showToast("Pesquise um endereço.");
            }
        });

        ImageButton deviceLocation = findViewById(R.id.get_device_location);
        deviceLocation.setOnClickListener(v -> getDeviceLocation());

        routeManagerListener = new RouteManager.Listener() {
            @SuppressLint("DefaultLocale")
            public void onCalculateRouteFinished(RouteManager.Error errorCode,
                                                 List<RouteResult> result) {

                if (errorCode == RouteManager.Error.NONE && result.get(0).getRoute() != null) {
                    // create a map route object and place it on the map
                    mapRoute.add(new MapRoute(result.get(0).getRoute()));
                    mMap.addMapObject(mapRoute.get(mapRoute.size() - 1));

                    // Get the bounding box containing the route and zoom in (no animation)
                    GeoBoundingBox gbb = result.get(0).getRoute().getBoundingBox();
                    mMap.zoomTo(gbb, Map.Animation.NONE, Map.MOVE_PRESERVE_ORIENTATION);

//                textViewResult.setText(String.format("Route calculated with %d maneuvers.",
//                        result.get(0).getRoute().getManeuvers().size()));
                    showToast(String.format("Route calculated with %d maneuvers.",
                            result.get(0).getRoute().getManeuvers().size()));
                } else {
//                textViewResult.setText(
//                        String.format("Route calculation failed: %s", errorCode.toString()));
                    showToast(String.format("Route calculation failed: %s", errorCode.toString()));
                }
            }

            public void onProgress(int percentage) {
//            textViewResult.setText(String.format("... %d percent done ...", percentage));
            }
        };

//        autocompleteSupportFragment = (AutocompleteSupportFragment) getSupportFragmentManager().findFragmentById(R.id.am_places_autocomplete);
    }

    private void showMapMenuFragment() {
        if (userSearch != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            MapMenuFragment dialog;
            dialog = MapMenuFragment.newInstance(userSearch.getAddress(), userSearch.getCoordinate(), -1);
            dialog.setCancelable(false);
            dialog.show(fragmentManager, MapMenuFragment.MAP_MENU_TAG);
        } else {
            showToast("Pesquise um endereço.");
        }
    }


    public void getDirections() {
//        showToast("getDirections: "+ userGeo +" plus "+ userSearch);
//        Log.d(TAG, "getDirections: " + userGeo + " plus " + userSearch);
        if (userGeo != null && userSearch != null) {
            // 1. clear previous results
//        textViewResult.setText("");
            clearDirections();

            // 2. Initialize RouteManager
            RouteManager routeManager = new RouteManager();

            // 3. Select routing options
            RoutePlan routePlan = new RoutePlan();

            RouteOptions routeOptions = new RouteOptions();
            routeOptions.setTransportMode(RouteOptions.TransportMode.CAR);
            routeOptions.setRouteType(RouteOptions.Type.FASTEST);
            routePlan.setRouteOptions(routeOptions);

            // 4. Select Waypoints for your routes
            // START: Nokia, Burnaby
            routePlan.addWaypoint(userGeo.getCoordinate());

            // END: Airport, YVR
            routePlan.addWaypoint(userSearch.getCoordinate());

            // 5. Retrieve Routing information via RouteManagerEventListener
            RouteManager.Error error = routeManager.calculateRoute(routePlan, routeManagerListener);
            if (error != RouteManager.Error.NONE) {
                Toast.makeText(getApplicationContext(),
                        "Route calculation failed with: " + error.toString(), Toast.LENGTH_SHORT)
                        .show();
            }
        } else {
//            showToast("Pesquise um endereço.");
        }
    }


    private void initMapEngine(Bundle savedInstanceState) {
        Log.d(TAG, "initGoogleMap: initializing map.");
        Bundle mapViewBundle = null;
        if (savedInstanceState != null) {
            Log.d(TAG, "initGoogleMap() called with: savedInstanceState = [" + savedInstanceState.toString() + "]");
            mapViewBundle = savedInstanceState.getBundle(MAPVIEW_BUNDLE_KEY);
        }
        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map_fragment);
        assert mapFragment != null;
        mapFragment.onCreate(mapViewBundle);
        mapFragment.init(error -> {
            if (error == OnEngineInitListener.Error.NONE) {
                // retrieve a reference of the map from the map fragment
                mMap = mapFragment.getMap();
                // Set the map center coordinate to the Vancouver region (no animation)
                mMap.setCenter(new GeoCoordinate(-22.876564, -47.055836, 0.0),
                        Map.Animation.NONE);
                // Set the map zoom level to the average between min and max (no animation)
                mMap.setZoomLevel((mMap.getMaxZoomLevel() + mMap.getMinZoomLevel()) / 2);
            } else {
                Log.e(TAG, "Cannot initialize SupportMapFragment (" + error + ")");
            }
        });
        getDeviceLocation();

        MapEngine.getInstance().init(this, error -> Log.d(TAG, "onEngineInitializationCompleted: Success"));
    }

    private void startLocationService() {
        if (!isLocationServiceRunning()) {
            Intent serviceIntent = new Intent(this, LocationService.class);
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {

                MainActivityNavMap.this.startForegroundService(serviceIntent);
            } else {
                startService(serviceIntent);
            }
        }
    }

    private boolean isLocationServiceRunning() {
        ActivityManager manager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if ("com.example.caronasolidaria.services.LocationService".equals(service.service.getClassName())) {
                Log.d(TAG, "isLocationServiceRunning: location service is already running.");
                return true;
            }
        }
        Log.d(TAG, "isLocationServiceRunning: location service is not running.");
        return false;
    }


    private void saveUserLocation() {
        if (userGeo != null) {
            DocumentReference locationRef = firebaseFirestore
                    .collection(getString(R.string.collection_user_locations))
                    .document(Objects.requireNonNull(FirebaseAuth.getInstance().getUid()));
            locationRef.set(userGeo).addOnCompleteListener(task -> {
                if (task.isSuccessful()) {
                    Log.d(TAG, "saveUserLocation:" + FirebaseAuth.getInstance().getCurrentUser().getDisplayName());
                }
            });
        }
    }


    private void setupPlacesAutoComplete() {
        Log.d(TAG, "setupPlacesAutoComplete: preparing auto complete search box");

        mSearchText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(final CharSequence s, int start, int before, int count) {
//                Log.d(TAG, "onTextChanged: "+ s);
                String query = s + "";
                GeocodeRequest geocodeRequest = new GeocodeRequest(query);
                GeoCoordinate coordinate = new GeoCoordinate(-22.875798, -47.056988);
                geocodeRequest.setSearchArea(coordinate, 5000);
                if (s.length() > 2) {
                    geocodeRequest.execute((results, errorCode) -> {
                        if (errorCode == ErrorCode.NONE) {
                            /*
                             * From the result object, we retrieve the location and its coordinate and
                             * display to the screen. Please refer to HERE Android SDK doc for other
                             * supported APIs.
                             */
                            Location geo = null;
                            StringBuilder sb = new StringBuilder();
                            for (GeocodeResult result : results) {
                                sb.append("For input: " + result.getLocation().getAddress() + " output: ");
                                sb.append(result.getLocation().getCoordinate().toString());
                                sb.append("\n");
                                geo = result.getLocation();
                                result.getLocation().getAddress();
                            }
                            Log.d(TAG, "onComplete: " + sb.toString());
                            if (geo != null) {
                                userSearch = geo;
                            }
                        } else {
                            Log.d(TAG, "ERROR:Geocode Request returned error code:" + errorCode);
                        }
                    });
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
//                Log.d(TAG, "afterTextChanged: "+ s);
            }
        });

        mSearchText.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_SEARCH
                    || actionId == EditorInfo.IME_ACTION_GO
                    || actionId == EditorInfo.IME_ACTION_DONE
                    || event.getAction() == KeyEvent.ACTION_DOWN
                    || event.getAction() == KeyEvent.KEYCODE_ENTER) {

                if (userSearch != null) {
                    moveCamera(userSearch, DEFAULT_ZOOM);
                }
            }
            return false;
        });

    }




    @Override
    protected void onStop() {
        super.onStop();
        mapFragment.onStop();
    }




    private void clearDirections() {
        if (mMap != null && mapRoute != null) {
            for (MapRoute route : mapRoute) {
                mMap.removeMapObject(route);
            }
            mapRoute = new ArrayList<>();
        }

    }

    private Location convertLocation(android.location.Location location) {
        return new Location(new GeoCoordinate(location.getLatitude(), location.getLongitude()));
    }


    @SuppressLint("MissingPermission")
    private void getDeviceLocation() {
        clearDirections();
        Log.d(TAG, "getDeviceLocation: getting device location");
        if (!getLocationPermission(this)) return;

        final Task<android.location.Location> location = fusedLocationProviderClient.getLastLocation();

        location.addOnCompleteListener(task -> {
            android.location.Location local = task.getResult();
            if (task.isSuccessful() && local != null) {
                userGeo = convertLocation(local);
                moveCamera(userGeo, DEFAULT_ZOOM);
                saveUserLocation();
            } else {
                Timer timer = new Timer();
                if (count % 5 == 0)
                    showToast(getString(R.string.tryingToEstabilishLocation));
                timer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        Log.d(TAG, "onComplete: trying location again for the " + count++);
                        getDeviceLocation();
                    }
                }, 6000);
            }
        });
    }


    private void moveCamera(com.here.android.mpa.search.Location location, Double zoom) {
        double lat = location.getCoordinate().getLatitude();
        double lng = location.getCoordinate().getLongitude();
        Log.d(TAG, "moveCamera() called with: latitude, longitude  = [" + lat + "," + lng + "], zoom = [" + zoom + "]");

        if (mMap != null) {
            mMap.setZoomLevel(zoom);
            mMap.setCenter(location.getCoordinate(), Map.Animation.LINEAR);
            if (marker != null) {
                mMap.removeMapObject(marker);
            } else {
                marker = new MapMarker();
            }
            marker.setCoordinate(location.getCoordinate());
            mMap.addMapObject(marker);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(TAG, "onActivityResult: called.");
        if (requestCode == PERMISSION_REQUEST_ENABLE_GPS) {
            if (!mLocationPermissionGranted) {
                getLocationPermission(this);
//                getUserDetails();
                Log.d(TAG, "onActivityResult: successful calling method getUserDetails()");
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        Log.d(TAG, "onRequestPermissionsResult: called.");
        mLocationPermissionGranted = false;
        if (requestCode == LOCATION_PERMISSION_REQUEST_CODE) {
            if (grantResults.length > 0) {
                for (int grantResult : grantResults) {
                    if (grantResult != PackageManager.PERMISSION_GRANTED) {
                        mLocationPermissionGranted = false;
                        Log.d(TAG, "onRequestPermissionsResult: permission failed");
                        return;
                    }
                }
                Log.d(TAG, "onRequestPermissionsResult: permission granted");
                mLocationPermissionGranted = true;
//                getUserDetails();
                Log.d(TAG, "onRequestPermissionsResult: successful calling method getUserDetails()");
            }
        }
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        TextView userNameHeader = findViewById(R.id.user_name);
        userNameHeader.setText(userName);
        TextView emailNavHeader = findViewById(R.id.user_email);
        emailNavHeader.setText(email);
        return true;
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);

        Bundle mapViewBundle = outState.getBundle(MAPVIEW_BUNDLE_KEY);
        if (mapViewBundle == null) {
            mapViewBundle = new Bundle();
            outState.putBundle(MAPVIEW_BUNDLE_KEY, mapViewBundle);
        }

        mapFragment.onSaveInstanceState(mapViewBundle);
    }


    @Override
    public void onPause() {
        mapFragment.onPause();
        super.onPause();
    }

    @Override
    public void onDestroy() {
        mapFragment.onDestroy();
        super.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapFragment.onLowMemory();
    }

    public void makeDialog(String msg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(msg).setCancelable(false)
                .setPositiveButton("Ok", (dialog, which) -> finish());
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.menu_action_exit) {
            singOut();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    //    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_menu_my_account) {
            goToUserActivity();
        } else if (id == R.id.nav_menu_my_vehicle) {
            goToVehicleActivity();
        } else if (id == R.id.search_carpooling) {
            goToSearchActivity();
        } else if (id == R.id.info_acitivy) {
            goToInfoActivity();
        }
//        else if (id == R.id.nav_menu_travel_historic) {
//            goToTravelHistory();
//        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void singOut() {
        if (FirebaseAuth.getInstance() != null) {
            AuthUI.getInstance().signOut(MainActivityNavMap.this)
                    .addOnCompleteListener(task -> goToSingInActivity());
        }
    }

    private void goToVehicleActivity() {
        startActivity(new Intent(MainActivityNavMap.this, VehicleActivity.class));
    }

    private void goToTravelHistory() {
        startActivity(new Intent(MainActivityNavMap.this, TravelHistoricActivity.class));
    }

    private void goToSearchActivity() {
        startActivity(new Intent(MainActivityNavMap.this, SearchActivity.class));
    }

    private void goToUserActivity() {
        startActivity(new Intent(MainActivityNavMap.this, AccountActivity.class));
    }

    private void goToInfoActivity() {
        startActivity(new Intent(MainActivityNavMap.this, InfoActivity.class));
    }

    private void goToSingInActivity() {
        startActivity(new Intent(MainActivityNavMap.this, SplashScreenActivity.class));
        finish();
    }

}
//    private void getUserDetails() {
//        if (userGeo == null) {
//            mUserLocation = new UserLocation();
//            DocumentReference userRef = firebaseFirestore.collection(getString(R.string.collection_users))
//                    .document(Objects.requireNonNull(FirebaseAuth.getInstance().getUid()));
//
//            userRef.get().addOnCompleteListener(task -> {
//                if (task.isSuccessful()) {
//                    FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
//                    mUserLocation.setUser(user);
//                    ((UserClient) getApplicationContext()).setUser(user);
//                    assert user != null;
//                    Log.d(TAG, "getUserDetails: task is successful with the user:" + user.getEmail());
//                    getDeviceLocation();
//                }
//            });
//        } else {
//            getDeviceLocation();
//        }
//    }


//    private android.location.Location getLastKnownLocation() {
//        android.location.Location location = null;
//        LocationManager mLocationManager = (LocationManager) getApplicationContext().getSystemService(LOCATION_SERVICE);
//        List<String> providers = mLocationManager.getProviders(true);
//        android.location.Location bestLocation = null;
//        for (String provider : providers) {
//            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
//                location = mLocationManager.getLastKnownLocation(provider);
//            }
//            if (location == null) {
//                continue;
//            }
//            if (bestLocation == null || location.getAccuracy() < bestLocation.getAccuracy()) {
//                bestLocation = location;
//            }
//        }
//        return bestLocation;
//    }