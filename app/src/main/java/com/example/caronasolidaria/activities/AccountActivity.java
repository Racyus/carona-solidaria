package com.example.caronasolidaria.activities;

import android.content.Intent;
import android.location.Address;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.caronasolidaria.R;
import com.example.caronasolidaria.models.Aluno;
import com.example.caronasolidaria.models.UserClient;
import com.example.caronasolidaria.utils.Constants;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.Objects;

public class AccountActivity extends BaseActivity {

    private static final String TAG = "AccountActivity";

    private DocumentReference userReference = null;

    private ImageButton backButton = null;
    private Button saveButton = null;

    private TextView userName = null;
    private TextView email = null;
    private EditText phone = null;
    private EditText RA = null;
    private EditText Campus = null;
    private EditText Curso = null;
//    private TextInputLayout realNameLayout, userNameLayout, emailLayout, RALayout, CampusLayout, CursoLayout;

    private void bridge() {
        userReference = FirebaseFirestore.getInstance().collection(getString(R.string.collection_users))
                .document(getUid())
                .collection(getString(R.string.collection_user_info))
                .document(getString(R.string.user_info));
        userName = findViewById(R.id.ac_account_real_name_edit_text);
        email = findViewById(R.id.ac_account_email_edit_text);
        phone = findViewById(R.id.ac_phone_text);
        RA = findViewById(R.id.ac_account_ra_edit_text);
        Campus = findViewById(R.id.ac_account_campus_edit_text);
        Curso = findViewById(R.id.ac_account_course_edit_text);


//        realNameLayout = findViewById(R.id.ac_account_real_name_layout);
//        userNameLayout = findViewById(R.id.ac_account_user_name_layout);
//        emailLayout = findViewById(R.id.ac_account_email_layout);
//        RALayout = findViewById(R.id.ac_account_ra_layout);
//        CampusLayout = findViewById(R.id.ac_account_campus_layout);
//        CursoLayout = findViewById(R.id.ac_account_course_layout);

        backButton = findViewById(R.id.ac_back);
        saveButton = findViewById(R.id.ac_account_save_button);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account);
        customStatusBar();

        bridge();

        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    userReference.set(getAluno());
                    showToast(getString(R.string.save_success));
                    startActivity(new Intent(AccountActivity.this, AddressActivity.class));
                    finish();
                } catch (Exception e) {
                    showToast(getString(R.string.save_error));
                    e.printStackTrace();
                }
            }
        });

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        hideSoftKeyboard();

        showProgressDialog();

        Address address = new Address(Constants.Brazil);

        address.setAddressLine(409, "Rua Elidia Ana de Campos");

    }

    @Override
    protected void onResume() {
        super.onResume();
        getUserFromFirebase();
    }

    private Aluno getAluno() {
        Aluno aluno = new Aluno();
        aluno.setRA(RA.getText().toString());
        aluno.setTelefone(phone.getText().toString());
        aluno.setCampus(Campus.getText().toString());
        aluno.setCurso(Curso.getText().toString());
        return aluno;
    }

    private void getUserFromFirebase() {
        userReference.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    if (Objects.requireNonNull(task.getResult()).toObject(Aluno.class) != null) {
                        fillScreen(Objects.requireNonNull(task.getResult().toObject(Aluno.class)));
                    }else{
                        hideProgressDialog();
                        //TODO logar
                        Toast.makeText(AccountActivity.this, "Null Object", Toast.LENGTH_LONG).show();
                    }
                    fillUserInfo();
                }
            }
        });
    }

    private void fillUserInfo(){
        userName.setText(FirebaseAuth.getInstance().getCurrentUser().getDisplayName());
        email.setText(FirebaseAuth.getInstance().getCurrentUser().getEmail());

    }

    private void fillScreen(Aluno aluno) {
        RA.setText(aluno.getRA());
        phone.setText(aluno.getTelefone());
        Campus.setText(aluno.getCampus());
        Curso.setText(aluno.getCurso());
        hideProgressDialog();
    }

}
