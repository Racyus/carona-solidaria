package com.example.caronasolidaria.activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.example.caronasolidaria.R;
import com.firebase.ui.auth.AuthUI;
import com.firebase.ui.auth.IdpResponse;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.Objects;

import static com.example.caronasolidaria.utils.Constants.MY_REQUEST_CODE;
import static com.example.caronasolidaria.utils.Constants.providers;

public class SingIn extends BaseActivity {

    private static final String TAG = "SignInActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        hideSoftKeyboard();
    }

    private void goToMainActivity() {
        Log.d(TAG, "goToMainActivity: changing activity.");
        startActivity(new Intent(SingIn.this, MainActivityNavMap.class));
        finish();
    }


    @Override
    protected void onResume() {
        super.onResume();
        if (FirebaseAuth.getInstance().getCurrentUser() != null) {
            Log.d(TAG, "onResume: FirebaseUser already logged" + FirebaseAuth.getInstance().getCurrentUser().getEmail());
            goToMainActivity();
        } else {
            showSignInOption();
        }
    }

    private void showSignInOption() {
        startActivityForResult(
                AuthUI.getInstance()
                        .createSignInIntentBuilder()
                        .setAvailableProviders(providers)
                        .setTheme(R.style.FirebaseLoginTheme)
                        .setLogo(R.drawable.carona_solidaria_logo_branco)
                        .setIsSmartLockEnabled(false)
                        .build()
                , MY_REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == MY_REQUEST_CODE) {
            IdpResponse response = IdpResponse.fromResultIntent(data);
            if (resultCode == RESULT_OK) {
                DocumentReference userReference = FirebaseFirestore.getInstance().collection(getString(R.string.collection_users))
                        .document(getUid());
                userReference.set(Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()));
                Log.d(TAG, "onActivityResult: logging with user:" + FirebaseAuth.getInstance().getCurrentUser().getEmail());
                goToMainActivity();
            } else {
                Toast.makeText(this, "" + Objects.requireNonNull(Objects.requireNonNull(response).getError()).getMessage(), Toast.LENGTH_SHORT).show();
                Log.d(TAG, "onActivityResult: " + response.getError());
            }
        }
    }
}
