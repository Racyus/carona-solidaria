package com.example.caronasolidaria.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.example.caronasolidaria.R;
import com.example.caronasolidaria.holders.CustomSpinner;
import com.example.caronasolidaria.holders.CustomSpinnerAdapter;
import com.example.caronasolidaria.models.Carpooling;
import com.example.caronasolidaria.models.Vehicle;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;
import java.util.Objects;

public class CreateCarpooling extends BaseActivity {
    private static final String TAG = "CreateCarpooling";

    private TextView city = null;
    private TextView institution = null;
    private TextView institution_city = null;

    private TextView board = null;
    private TextView expenditure_km = null;
    private TextView expenditure_month = null;

    private Spinner comesAndGoes = null;
    private Spinner iAm = null;
    private Spinner period = null;
    private Spinner vehicleSpinner = null;

    private void bridge() {
        city = findViewById(R.id.create_carpooling_city_textView);
        institution = findViewById(R.id.create_carpooling_institution_textView);
        institution_city = findViewById(R.id.create_carpooling_institution_city_textView);
        board = findViewById(R.id.create_carpooling_board_textView);
        expenditure_km = findViewById(R.id.create_carpooling_km_expenditure_textView);
        expenditure_month = findViewById(R.id.create_carpooling_monthly_expenditure_textView);


        ArrayList<CustomSpinner> list1 = new ArrayList<>();
        CharSequence[] strings = getApplicationContext().getResources().getTextArray(R.array.comesAndGoes);
        for (CharSequence string2 : strings) {
            list1.add(new CustomSpinner(string2 + ""));
        }
        CustomSpinnerAdapter customAdapter1 = new CustomSpinnerAdapter(getApplicationContext(), list1);
        comesAndGoes = findViewById(R.id.create_carpooling_comesAndGoes_spinner);
        comesAndGoes.setAdapter(customAdapter1);
        comesAndGoes.setPopupBackgroundResource(R.color.cs_branco);

        ArrayList<CustomSpinner> list2 = new ArrayList<>();
        strings = getApplicationContext().getResources().getTextArray(R.array.type);
        for (CharSequence string1 : strings) {
            list2.add(new CustomSpinner(string1 + ""));
        }
        CustomSpinnerAdapter customAdapter2 = new CustomSpinnerAdapter(getApplicationContext(), list2);
        iAm = findViewById(R.id.create_carpooling_iOffer_spinner);
        iAm.setAdapter(customAdapter2);
        iAm.setPopupBackgroundResource(R.color.cs_branco);

        ArrayList<CustomSpinner> list3 = new ArrayList<>();
        strings = getApplicationContext().getResources().getTextArray(R.array.period);
        for (CharSequence string : strings) {
            list3.add(new CustomSpinner(string + ""));
        }
        CustomSpinnerAdapter customAdapter3 = new CustomSpinnerAdapter(getApplicationContext(), list3);
        period = findViewById(R.id.create_carpooling_period_spinner);
        period.setAdapter(customAdapter3);
        period.setPopupBackgroundResource(R.color.cs_branco);

        vehicleSpinner = findViewById(R.id.create_carpooling_vehicle_spinner);
    }

    private void fillCard(Carpooling cp) {
        city.setText(cp.getUserHouse().getCity());
        institution.setText(cp.getAluno().getCampus());
        institution_city.setText(cp.getUserInstitution().getCity());
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result_carpooling);
        setTitle("CreateCarpooling");
        customStatusBar();
        bridge();

    }

    private void displayVehicle(Vehicle vehicle) {
        board.setText(vehicle.getBoard());
        expenditure_km.setText(vehicle.getKmExpenditure());
        expenditure_month.setText(vehicle.getMonthlyExpenditure());
    }

    @Override
    protected void onResume() {
        super.onResume();
        final Carpooling carpooling = (Carpooling) getIntent().getSerializableExtra(SearchActivity.CARPOOL);
        final Vehicle vehicle1 = (Vehicle) getIntent().getSerializableExtra(SearchActivity.VEHICLE1);
        final Vehicle vehicle2 = (Vehicle) getIntent().getSerializableExtra(SearchActivity.VEHICLE2);
        final String uid = (String) getIntent().getSerializableExtra(SearchActivity.UID);
        fillCard(carpooling);


        ArrayList<CustomSpinner> list = new ArrayList<>();
        if(vehicle1!=null){
            list.add(new CustomSpinner(vehicle1.getModel()));
        }
        if(vehicle2!=null){
            list.add(new CustomSpinner(vehicle2.getModel()));
        }
        CustomSpinnerAdapter customAdapter = new CustomSpinnerAdapter(getApplicationContext(), list);
        vehicleSpinner.setAdapter(customAdapter);
        vehicleSpinner.setPopupBackgroundResource(R.color.cs_branco);
        vehicleSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (id == 0) {
                    displayVehicle(vehicle1);
                    carpooling.setVehicle(vehicle1);
                } else {
                    displayVehicle(vehicle2);
                    carpooling.setVehicle(vehicle2);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        Button cancel = findViewById(R.id.create_carpooling_cancel);
        cancel.setOnClickListener(v -> {
            setResult(RESULT_CANCELED);
            finish();
        });

        Button create = findViewById(R.id.create_carpooling_create);
        create.setOnClickListener(v -> {
            carpooling.setGoing_or_coming(((CustomSpinner) comesAndGoes.getSelectedItem()).getSpinnerTextView());
            carpooling.setType(((CustomSpinner) iAm.getSelectedItem()).getSpinnerTextView());
            carpooling.setPeriod(((CustomSpinner) period.getSelectedItem()).getSpinnerTextView());
            carpooling.setUid(getUid());
            carpooling.setEmail(Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getEmail());
            DocumentReference carpoolsRef = FirebaseFirestore.getInstance().collection(getString(R.string.collection_carpoolings))
                    .document(getUid());
            carpoolsRef.set(carpooling);
            Toast.makeText(getApplicationContext(), "Criado com sucesso", Toast.LENGTH_LONG).show();
            setResult(RESULT_OK);
            finish();
        });
    }
}