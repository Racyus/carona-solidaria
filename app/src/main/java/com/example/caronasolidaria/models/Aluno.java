package com.example.caronasolidaria.models;

import java.io.Serializable;


public class Aluno implements Serializable {

    private String RA = null;
    private String Campus = null;
    private String Curso = null;
    private String telefone = null;

    public Aluno() {
        super();
    }

    public String getRA() {
        return RA;
    }

    public void setRA(String RA) {
        this.RA = RA;
    }

    public String getCampus() {
        return Campus;
    }

    public void setCampus(String campus) {
        Campus = campus;
    }

    public String getCurso() {
        return Curso;
    }

    public void setCurso(String curso) {
        Curso = curso;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }


}
