package com.example.caronasolidaria.models;


import java.io.Serializable;

public class Latlog implements Serializable {

    private Double latitude;
    private Double longitude;

    public Latlog() {
    }

    public Latlog(Double latitude, Double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public Double getLongitude() {
        return longitude;
    }
}
