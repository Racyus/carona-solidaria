package com.example.caronasolidaria.models;

import java.io.Serializable;

public class MyUserAddress implements Serializable {

    private String street = null;
    private String houseNumber = null;
    private String postalCode = null;
    private String city = null;
    private String state = null;
    private String countryName = null;
    private Latlog latlog = null;

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getHouseNumber() {
        return houseNumber;
    }

    public void setHouseNumber(String houseNumber) {
        this.houseNumber = houseNumber;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public Latlog getLatlog() {
        return latlog;
    }

    public void setLatlog(Latlog latlog) {
        this.latlog = latlog;
    }
}
