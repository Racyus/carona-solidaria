package com.example.caronasolidaria.models;

import androidx.annotation.NonNull;

import java.io.Serializable;

public class CsRequest implements Serializable {

    private String name;
    private String ra;
    private String city;
    private String phone;
    private String institution;
    private String institutionCity;
    private String course;
    private String requestUid;

    public String getName() {
        return name;
    }

    public CsRequest setName(String name) {
        this.name = name;
        return this;
    }

    public String getRa() {
        return ra;
    }

    public CsRequest setRa(String ra) {
        this.ra = ra;
        return this;
    }

    public String getCity() {
        return city;
    }

    public CsRequest setCity(String city) {
        this.city = city;
        return this;
    }

    public String getPhone() {
        return phone;
    }

    public CsRequest setPhone(String phone) {
        this.phone = phone;
        return this;
    }

    public String getInstitution() {
        return institution;
    }

    public CsRequest setInstitution(String institution) {
        this.institution = institution;
        return this;
    }

    public String getInstitutionCity() {
        return institutionCity;
    }

    public CsRequest setInstitutionCity(String institutionCity) {
        this.institutionCity = institutionCity;
        return this;
    }

    public String getCourse() {
        return course;
    }

    public CsRequest setCourse(String course) {
        this.course = course;
        return this;
    }

    public String getRequestUid() {
        return requestUid;
    }

    public CsRequest setRequestUid(String requestUid) {
        this.requestUid = requestUid;
        return this;
    }

    @NonNull
    @Override
    public String toString() {
        return "Name: "+ name+ "  - City:" +city+"  - Phone: "+ phone;
    }
}
