package com.example.caronasolidaria.models;

import java.io.Serializable;

public class Vehicle implements Serializable {

    private String model= null;
    private String board= null;
    private String color= null;
    private String KmExpenditure= null;
    private String monthlyExpenditure= null;

    public Vehicle() {
    }

    public String getModel() {
        if(model==null){
            return "Não inf.";
        }
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getBoard() {
        return board;
    }

    public void setBoard(String board) {
        this.board = board;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getKmExpenditure() {
        return KmExpenditure;
    }

    public void setKmExpenditure(String kmExpenditure) {
        KmExpenditure = kmExpenditure;
    }

    public String getMonthlyExpenditure() {
        return monthlyExpenditure;
    }

    public void setMonthlyExpenditure(String monthlyExpenditure) {
        this.monthlyExpenditure = monthlyExpenditure;
    }


}
