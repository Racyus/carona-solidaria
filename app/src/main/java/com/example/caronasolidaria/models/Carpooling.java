package com.example.caronasolidaria.models;

import java.io.Serializable;

public class Carpooling implements Serializable {

    private String name = null;
    private String uid = null;
    private Aluno aluno = null;
    private MyUserAddress userHouse = null;
    private MyUserAddress userInstitution = null;
    private Vehicle vehicle = null;
    private String email = null;
    private String going_or_coming = null;
    private String type = null;
    private String period = null;

    public Carpooling() {
    }

    public Carpooling(String name, Aluno aluno, MyUserAddress userHouse, MyUserAddress userInstitution) {
        this.name = name;
        this.aluno = aluno;
        this.userHouse = userHouse;
        this.userInstitution = userInstitution;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Aluno getAluno() {
        return aluno;
    }

    public void setAluno(Aluno aluno) {
        this.aluno = aluno;
    }

    public MyUserAddress getUserHouse() {
        return userHouse;
    }

    public void setUserHouse(MyUserAddress userHouse) {
        this.userHouse = userHouse;
    }

    public MyUserAddress getUserInstitution() {
        return userInstitution;
    }

    public void setUserInstitution(MyUserAddress userInstitution) {
        this.userInstitution = userInstitution;
    }

    public Vehicle getVehicle() {
        return vehicle;
    }

    public void setVehicle(Vehicle vehicle) {
        this.vehicle = vehicle;
    }

    public String getGoing_or_coming() {
        return going_or_coming;
    }

    public void setGoing_or_coming(String going_or_coming) {
        this.going_or_coming = going_or_coming;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

}
