package com.example.caronasolidaria.models;


import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.ServerTimestamp;
import com.here.android.mpa.search.Location;

import java.io.Serializable;
import java.util.Date;

public class UserLocation implements Serializable {

    private Location geo_point = null;
    private @ServerTimestamp Date timestamp;
    private FirebaseUser user;

    public UserLocation() {
    }

    public UserLocation(Location geo_point, Date timestamp, FirebaseUser user) {
        this.geo_point = geo_point;
        this.timestamp = timestamp;
        this.user = user;
    }


    public Location getGeo_point() {
        return geo_point;
    }

    public void setGeo_point(Location geo_point) {
        this.geo_point = geo_point;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public FirebaseUser getUser() {
        return user;
    }

    public void setUser(FirebaseUser user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "UserLocation{" +
                "coordinates:[" + geo_point.getCoordinate().getLatitude() +", "+ geo_point.getCoordinate().getLongitude() +
                "], timestamp=" + timestamp +
                ", user=" + user +
                '}';
    }
}
