package com.example.caronasolidaria.models;

import android.app.Application;

import com.google.firebase.auth.FirebaseUser;

public class UserClient extends Application {


    private String token;
    private FirebaseUser user = null;

    public FirebaseUser getUser() {
        return user;
    }

    public void setUser(FirebaseUser user) {
        this.user = user;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
