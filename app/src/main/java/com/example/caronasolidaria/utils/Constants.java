package com.example.caronasolidaria.utils;

import android.Manifest;

import com.firebase.ui.auth.AuthUI;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;

public abstract class Constants {
    public static final Locale Brazil = new Locale("pt", "BR");
    static final String FINE_LOCATION = Manifest.permission.ACCESS_FINE_LOCATION;
    static final String COURSE_LOCATION = Manifest.permission.ACCESS_COARSE_LOCATION;
    public static final int LOCATION_PERMISSION_REQUEST_CODE = 1234;
    public static final double DEFAULT_ZOOM = 17.5;
    public static final int ERROR_DIALOG_REQUEST = 9001;
    public static final int PERMISSION_REQUEST_ENABLE_GPS= 9002;
    public static final String MAPVIEW_BUNDLE_KEY = "MapViewBundleKey";
    public static final int MY_REQUEST_CODE = 8798;
    public static List<AuthUI.IdpConfig> providers = Arrays.asList(
                new AuthUI.IdpConfig.EmailBuilder().build() //Email Builder
//                ,new AuthUI.IdpConfig.GoogleBuilder().build() //Google Builder
        );

}
