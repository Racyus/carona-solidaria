package com.example.caronasolidaria.utils;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.provider.Settings;
import android.util.Log;
import android.view.WindowManager;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.example.caronasolidaria.R;

import java.util.Objects;

import static com.example.caronasolidaria.utils.Constants.COURSE_LOCATION;
import static com.example.caronasolidaria.utils.Constants.FINE_LOCATION;
import static com.example.caronasolidaria.utils.Constants.LOCATION_PERMISSION_REQUEST_CODE;
import static com.example.caronasolidaria.utils.Constants.PERMISSION_REQUEST_ENABLE_GPS;

public abstract class PermissionUtils {

    private static final String TAG = "PermissionUtils";

    public static Boolean getLocationPermission(Activity activity) {
        Log.d(TAG, "getLocationPermission() called with: activity = [" + activity.getLocalClassName() + "]");

        String[] permissions = {Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION};

        if (ContextCompat.checkSelfPermission(activity.getApplicationContext(), FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(activity.getApplicationContext(), COURSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            Log.d(TAG, "getLocationPermission: permissions granted.");
            return true;
        } else {
            ActivityCompat.requestPermissions(activity,
                    permissions,
                    LOCATION_PERMISSION_REQUEST_CODE);
        }
        Log.d(TAG, "getLocationPermission: permissions refused.");
        return false;
    }

    public static Boolean isMapsEnabled(Activity activity) {
        Log.d(TAG, "isMapsEnabled() called with: activity = [" + activity.getLocalClassName() + "]");
        final LocationManager manager = (LocationManager) activity.getSystemService(Context.LOCATION_SERVICE);

        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            Log.d(TAG, "isMapsEnabled: LocationManager.GPS_PROVIDER: NOT enabled");
            return false;
        }
        Log.d(TAG, "isMapsEnabled: LocationManager.GPS_PROVIDER: ENABLED");
        return true;
    }

    @Deprecated
    private static void buildAlertMessageNoGps(final Activity activity) {
        Log.d(TAG, "buildAlertMessageNoGps() called with: activity = [" + activity.getLocalClassName() + "]");

        final AlertDialog.Builder builder = new AlertDialog.Builder(activity.getApplicationContext());

        builder.setMessage(activity.getApplicationContext().getResources().getString(R.string.noGpsAlertDialog))
                .setCancelable(false)
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent enableGpsIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        activity.startActivityForResult(enableGpsIntent, PERMISSION_REQUEST_ENABLE_GPS);
                    }
                });

        final AlertDialog alert = builder.create();
        Objects.requireNonNull(alert.getWindow()).setType(WindowManager.LayoutParams.TYPE_TOAST);
        alert.show();

    }

}
