package com.example.caronasolidaria.adapters;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.caronasolidaria.R;
import com.example.caronasolidaria.activities.SearchActivity;
import com.example.caronasolidaria.models.Carpooling;
import com.example.caronasolidaria.holders.CarpoolingCard;
import com.example.caronasolidaria.models.CsRequest;
import com.example.caronasolidaria.utils.CsCallback;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.List;

public class CarpoolingAdapter extends RecyclerView.Adapter<CarpoolingCard> {

    private static final String TAG = "CarpoolingAdapter";
    private CsCallback callback;

    private String travel = null;
    private String request = null;
    private Carpooling user;

    private ViewGroup parent = null;

    private List<Carpooling> carpoolings;

    @NonNull
    @Override
    public CarpoolingCard onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        this.parent = parent;
        travel = parent.getContext().getString(R.string.collection_carpoolings);
        request = parent.getContext().getString(R.string.collection_requests);
        Log.d(TAG, "onCreateViewHolder: " + parent.getTransitionName());
        return new CarpoolingCard(LayoutInflater.from(parent.getContext()).inflate(R.layout.card_carpooling, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull CarpoolingCard holder, int position) {
        final Carpooling cp = carpoolings.get(position);
        holder.getUserCity().setText(cp.getUserHouse().getCity());
        holder.getComesAndGoes().setText(cp.getGoing_or_coming());
        holder.getInstitution().setText(cp.getAluno().getCampus());
        holder.getInstitutionCity().setText(cp.getUserInstitution().getCity());
        holder.getName().setText(cp.getName());
        holder.getType().setText(cp.getType());
        holder.getPeriod().setText(cp.getPeriod());
        if (cp.getUid().equals(FirebaseAuth.getInstance().getCurrentUser().getUid())) {
            holder.getRequest().setVisibility(View.GONE);
            holder.getDelete().setVisibility(View.VISIBLE);
            holder.getDelete().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DocumentReference carpoolsRef = FirebaseFirestore.getInstance().collection(travel)
                            .document(cp.getUid());
                    carpoolsRef.delete();
                    SearchActivity.updateScreen();
                    Toast.makeText(parent.getContext(), "Viagem deletada", Toast.LENGTH_SHORT).show();
                    callback.callbackReturn();
                }
            });
        } else {
            holder.getRequest().setVisibility(View.VISIBLE);
            holder.getDelete().setVisibility(View.GONE);
            holder.getRequest().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DocumentReference requestRf = FirebaseFirestore
                            .getInstance().collection(travel)
                            .document(cp.getUid())
                            .collection(request)
                            .document(FirebaseAuth.getInstance().getCurrentUser().getUid());
                    CsRequest rq = new CsRequest();
                    rq.setCity(user.getUserHouse().getCity())
                            .setCourse(user.getAluno().getCurso())
                            .setInstitution(user.getAluno().getCampus())
                            .setInstitutionCity(user.getUserInstitution().getCity())
                            .setName(user.getName())
                            .setPhone(user.getAluno().getTelefone())
                            .setRa(user.getAluno().getRA())
                            .setRequestUid(FirebaseAuth.getInstance().getCurrentUser().getUid());
                    requestRf.set(rq);
                    Toast.makeText(parent.getContext(), "Solicitação enviada", Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return carpoolings.size();
    }

    public CarpoolingAdapter(List<Carpooling> carpoolings, CsCallback callback, Carpooling user) {
        this.user=user;
        this.callback = callback;
        this.carpoolings = carpoolings;
    }
}
