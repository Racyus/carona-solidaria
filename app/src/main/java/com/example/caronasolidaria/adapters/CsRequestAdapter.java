package com.example.caronasolidaria.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.caronasolidaria.R;
import com.example.caronasolidaria.holders.CsRequestCard;
import com.example.caronasolidaria.models.CsRequest;
import com.example.caronasolidaria.utils.CsCallback;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.List;

public class CsRequestAdapter extends RecyclerView.Adapter<CsRequestCard> {

    private CsCallback callback;
    private List<CsRequest> list = null;

    private String travels = null;
    private String uid = null;
    private String requests = null;
    private String current = null;
    private String notification = null;


    @NonNull
    @Override
    public CsRequestCard onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        travels = parent.getContext().getString(R.string.collection_carpoolings);
        uid = FirebaseAuth.getInstance().getCurrentUser().getUid();
        requests = parent.getContext().getString(R.string.collection_requests);
        current = parent.getContext().getString(R.string.collection_current);
        notification = parent.getContext().getString(R.string.collection_request_notification);
        return new CsRequestCard(LayoutInflater.from(parent.getContext()).inflate(R.layout.card_csrequest, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull CsRequestCard holder, int position) {
        final CsRequest csRequest = list.get(position);

        final DocumentReference requestRf = FirebaseFirestore.getInstance()
                .collection(travels)
                .document(uid)
                .collection(requests)
                .document(csRequest.getRequestUid());

        final DocumentReference acceptRf = FirebaseFirestore.getInstance()
                .collection(travels)
                .document(uid)
                .collection(current)
                .document(csRequest.getRequestUid());

        final DocumentReference sendUserNotification = FirebaseFirestore.getInstance()
                .collection(travels)
                .document(csRequest.getRequestUid())
                .collection(notification)
                .document(uid);

        holder.getName().setText(csRequest.getName());
        holder.getCourse().setText(csRequest.getCourse());
        holder.getPhone().setText(csRequest.getPhone());
        holder.getRa().setText(csRequest.getRa());
        holder.getInstitution().setText(csRequest.getInstitution());
        holder.getInstitutionCity().setText(csRequest.getInstitutionCity());
        holder.getUserCity().setText(csRequest.getCity());
        holder.getAccept().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendUserNotification.set(csRequest);
                acceptRf.set(csRequest);
                requestRf.delete();
                callback.callbackReturn();
            }
        });
        holder.getRefuse().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestRf.delete();
                callback.callbackReturn();
            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public CsRequestAdapter(List<CsRequest> list, CsCallback callback) {
        this.callback = callback;
        this.list = list;
    }
}
