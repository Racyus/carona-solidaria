package com.example.caronasolidaria.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.caronasolidaria.R;
import com.example.caronasolidaria.holders.CsCurrentCard;
import com.example.caronasolidaria.models.CsRequest;
import com.example.caronasolidaria.utils.CsCallback;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.List;

public class CsCurrentAdapter extends RecyclerView.Adapter<CsCurrentCard> {

    private CsCallback callback;
    private List<CsRequest> list = null;

    private String travels = null;
    private String uid = null;
    private String requests = null;
    private String notification = null;
    private String current = null;

    @NonNull
    @Override
    public CsCurrentCard onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        travels = parent.getContext().getString(R.string.collection_carpoolings);
        uid = FirebaseAuth.getInstance().getCurrentUser().getUid();
        requests = parent.getContext().getString(R.string.collection_requests);
        current = parent.getContext().getString(R.string.collection_current);
        notification = parent.getContext().getString(R.string.collection_request_notification);
        return new CsCurrentCard(LayoutInflater.from(parent.getContext()).inflate(R.layout.card_csrequest, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull CsCurrentCard holder, int position) {
        final CsRequest csRequest = list.get(position);

        final DocumentReference acceptRf = FirebaseFirestore.getInstance()
                .collection(travels)
                .document(uid)
                .collection(current)
                .document(csRequest.getRequestUid());

        final DocumentReference sendUserNotification = FirebaseFirestore.getInstance()
                .collection(travels)
                .document(csRequest.getRequestUid())
                .collection(notification)
                .document(uid);

        holder.getName().setText(csRequest.getName());
        holder.getCourse().setText(csRequest.getCourse());
        holder.getPhone().setText(csRequest.getPhone());
        holder.getRa().setText(csRequest.getRa());
        holder.getInstitution().setText(csRequest.getInstitution());
        holder.getInstitutionCity().setText(csRequest.getInstitutionCity());
        holder.getUserCity().setText(csRequest.getCity());
        holder.getCancel().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendUserNotification.delete();
                acceptRf.delete();
                callback.callbackReturn();
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public CsCurrentAdapter(List<CsRequest> list, CsCallback callback) {
        this.callback = callback;
        this.list = list;
    }
}
